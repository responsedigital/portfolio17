<!-- Start Portfolio 17 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : A grid layout of projects in varying sizes. -->
@endif
<div class="portfolio-17"  is="fir-portfolio-17">
  <script type="application/json">
      {
          "options": @json($options)
      }
  </script>
  <div class="portfolio-17__wrap">
    @foreach($projects as $p)
    <div class="portfolio-17__project portfolio-17__project--{{ $p['project_size'] }}">
      <div class="portfolio-17__project-content">
        <img class="portfolio-17__project-image" src="{{ $p['image']['url'] }}" alt="{{ $p['title'] }}">
        <h5 class="portfolio-17__project-title">{{ $p['title'] }}</h5>
        <p class="portfolio-17__project-category">{{ $p['category'] }}</p>
      </div>
    </div>
    @endforeach
  </div>
</div>
<!-- End Portfolio 17 -->
