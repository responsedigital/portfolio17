module.exports = {
    'name'  : 'Portfolio 17',
    'camel' : 'Portfolio17',
    'slug'  : 'portfolio-17',
    'dob'   : 'Portfolio_17_1440',
    'desc'  : 'A grid layout of projects in varying sizes.',
}
