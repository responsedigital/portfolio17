<?php

namespace Fir\Pinecones\Portfolio17;

use Fir\Utils\GlobalFields;

class Schema
{
    public static function getACFLayout()
    {
        return [
            'name' => 'Portfolio17',
            'label' => 'Pinecone: Portfolio 17',
            'sub_fields' => [
                [
                    'label' => 'General',
                    'name' => 'generalTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Overview',
                    'name' => 'overview',
                    'type' => 'message',
                    'message' => "A grid layout of projects in varying sizes."
                ],
                [
                    'label' => 'Projects',
                    'name' => 'projectsTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Projects',
                    'name' => 'projects',
                    'type' => 'repeater',
                    'layout' => 'row',
                    'sub_fields' => [
                        [
                            'label' => 'Image',
                            'name' => 'image',
                            'type' => 'image'
                        ],
                        [
                            'label' => 'Title',
                            'name' => 'title',
                            'type' => 'text'
                        ],
                        [
                            'label' => 'Category',
                            'name' => 'category',
                            'type' => 'text'
                        ],
                        [
                            'label' => 'Project Link',
                            'name' => 'project_link',
                            'type' => 'link'
                        ],
                        [
                            'label' => 'Project Size',
                            'name' => 'project_size',
                            'type' => 'button_group',
                            'message' => 'Choose a size for this prject block to appear in the grid.',
                            'choices' => [
                                'small' => 'Small',
                                'med' => 'Medium',
                                'large' => 'Large',
                                'xlarge' => 'xLarge'
                            ],
                        ]
                    ]
                ],
                GlobalFields::getGroups(array(
                    'priorityGuides',
                    'ID'
                )),
                [
                    'label' => 'Options',
                    'name' => 'optionsTab',
                    'type' => 'tab',
                    'placement' => 'top',
                    'endpoint' => 0
                ],
                [
                    'label' => '',
                    'name' => 'options',
                    'type' => 'group',
                    'layout' => 'row',
                    'sub_fields' => [
                        GlobalFields::getOptions(array(
                            'hideComponent'
                        ))
                    ]
                ]
            ]
        ];
    }
}
