class Portfolio17 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.$ = $(this)
        this.props = this.getInitialProps()
        this.resolveElements()
      }

    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse($('script[type="application/json"]', this).text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initPortfolio17()
    }

    initPortfolio17 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: Portfolio 17")

      window.onload = this.resizeAllGridItems();
      window.addEventListener("resize", this.resizeAllGridItems);

      var allItems = document.getElementsByClassName("portfolio-17__project");
      for(var x=0;x<allItems.length;x++){
        this.resizeInstance(allItems[x]);
      }
    }

    resizeGridItem(item){
      var grid = document.getElementsByClassName("portfolio-17__wrap")[0];
      var rowHeight = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-auto-rows'));
      var rowGap = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-row-gap'));
      var rowSpan = Math.ceil((item.querySelector('.portfolio-17__project-content').getBoundingClientRect().height+rowGap)/(rowHeight+rowGap));
      item.style.gridRowEnd = "span "+rowSpan;
    }

    resizeAllGridItems(){
      var allItems = document.getElementsByClassName("portfolio-17__project");
      for(var x=0;x<allItems.length;x++){
        this.resizeGridItem(allItems[x]);
      }
    }

    resizeInstance(instance){
      this.resizeGridItem(instance);
    }

}

window.customElements.define('fir-portfolio-17', Portfolio17, { extends: 'div' })
